﻿namespace domi1819.UpCore.Config
{
    public enum UpdateBehavior
    {
        AutoInstall = 0,
        Indicate = 1,
        NeverCheck = 2,
    }
}
