﻿namespace domi1819.DarkControls
{
    internal interface IGlowComponent
    {
        int GlowX { get; }
        int GlowY { get; }
        int GlowW { get; }
        int GlowH { get; }
    }
}
