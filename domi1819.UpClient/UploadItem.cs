﻿namespace domi1819.UpClient
{
    internal class UploadItem
    {
        internal string FolderPath { get; set; }

        internal string FileName { get; set; }

        internal string FileExtension { get; set; }

        internal bool TemporaryFile { get; set; }
    }
}
