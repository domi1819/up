﻿namespace domi1819.UpClient.Forms
{
    partial class HotkeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HotkeyForm));
            this.uiUploadFileLabel = new System.Windows.Forms.Label();
            this.uiUploadFileKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiUploadScreenshotLabel = new System.Windows.Forms.Label();
            this.uiUploadScreenshotKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiUploadScreenAreaLabel = new System.Windows.Forms.Label();
            this.uiUploadScreenAreaKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiUploadClipbardLabel = new System.Windows.Forms.Label();
            this.uiUploadClipboardKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiShowFileDropAreaLabel = new System.Windows.Forms.Label();
            this.uiShowFileDropAreaKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiOpenStorageExplorer = new System.Windows.Forms.Label();
            this.uiSaveLocalScreenshotLabel = new System.Windows.Forms.Label();
            this.uiSaveLocalScreenAreaLabel = new System.Windows.Forms.Label();
            this.uiSaveLocalClipboardLabel = new System.Windows.Forms.Label();
            this.uiOpenStorageExplorerKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiSaveLocalScreenshotKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiSaveLocalScreenAreaKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.uiSaveLocalClipboardKeyBox = new domi1819.DarkControls.DarkKeyBox();
            this.SuspendLayout();
            // 
            // uiUploadFileLabel
            // 
            this.uiUploadFileLabel.AutoSize = true;
            this.uiUploadFileLabel.Location = new System.Drawing.Point(9, 12);
            this.uiUploadFileLabel.Name = "uiUploadFileLabel";
            this.uiUploadFileLabel.Size = new System.Drawing.Size(57, 13);
            this.uiUploadFileLabel.TabIndex = 2;
            this.uiUploadFileLabel.Text = "Upload file";
            // 
            // uiUploadFileKeyBox
            // 
            this.uiUploadFileKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiUploadFileKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiUploadFileKeyBox.Location = new System.Drawing.Point(12, 29);
            this.uiUploadFileKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiUploadFileKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiUploadFileKeyBox.Name = "uiUploadFileKeyBox";
            this.uiUploadFileKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiUploadFileKeyBox.TabIndex = 1;
            this.uiUploadFileKeyBox.TextValue = "";
            this.uiUploadFileKeyBox.UseSystemPasswordChar = false;
            // 
            // uiUploadScreenshotLabel
            // 
            this.uiUploadScreenshotLabel.AutoSize = true;
            this.uiUploadScreenshotLabel.Location = new System.Drawing.Point(9, 54);
            this.uiUploadScreenshotLabel.Name = "uiUploadScreenshotLabel";
            this.uiUploadScreenshotLabel.Size = new System.Drawing.Size(96, 13);
            this.uiUploadScreenshotLabel.TabIndex = 2;
            this.uiUploadScreenshotLabel.Text = "Upload screenshot";
            // 
            // uiUploadScreenshotKeyBox
            // 
            this.uiUploadScreenshotKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiUploadScreenshotKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiUploadScreenshotKeyBox.Location = new System.Drawing.Point(12, 71);
            this.uiUploadScreenshotKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiUploadScreenshotKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiUploadScreenshotKeyBox.Name = "uiUploadScreenshotKeyBox";
            this.uiUploadScreenshotKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiUploadScreenshotKeyBox.TabIndex = 2;
            this.uiUploadScreenshotKeyBox.TextValue = "";
            this.uiUploadScreenshotKeyBox.UseSystemPasswordChar = false;
            // 
            // uiUploadScreenAreaLabel
            // 
            this.uiUploadScreenAreaLabel.AutoSize = true;
            this.uiUploadScreenAreaLabel.Location = new System.Drawing.Point(9, 96);
            this.uiUploadScreenAreaLabel.Name = "uiUploadScreenAreaLabel";
            this.uiUploadScreenAreaLabel.Size = new System.Drawing.Size(100, 13);
            this.uiUploadScreenAreaLabel.TabIndex = 2;
            this.uiUploadScreenAreaLabel.Text = "Upload screen area";
            // 
            // uiUploadScreenAreaKeyBox
            // 
            this.uiUploadScreenAreaKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiUploadScreenAreaKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiUploadScreenAreaKeyBox.Location = new System.Drawing.Point(12, 113);
            this.uiUploadScreenAreaKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiUploadScreenAreaKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiUploadScreenAreaKeyBox.Name = "uiUploadScreenAreaKeyBox";
            this.uiUploadScreenAreaKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiUploadScreenAreaKeyBox.TabIndex = 3;
            this.uiUploadScreenAreaKeyBox.TextValue = "";
            this.uiUploadScreenAreaKeyBox.UseSystemPasswordChar = false;
            // 
            // uiUploadClipbardLabel
            // 
            this.uiUploadClipbardLabel.AutoSize = true;
            this.uiUploadClipbardLabel.Location = new System.Drawing.Point(9, 138);
            this.uiUploadClipbardLabel.Name = "uiUploadClipbardLabel";
            this.uiUploadClipbardLabel.Size = new System.Drawing.Size(87, 13);
            this.uiUploadClipbardLabel.TabIndex = 2;
            this.uiUploadClipbardLabel.Text = "Upload clipboard";
            // 
            // uiUploadClipboardKeyBox
            // 
            this.uiUploadClipboardKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiUploadClipboardKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiUploadClipboardKeyBox.Location = new System.Drawing.Point(12, 155);
            this.uiUploadClipboardKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiUploadClipboardKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiUploadClipboardKeyBox.Name = "uiUploadClipboardKeyBox";
            this.uiUploadClipboardKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiUploadClipboardKeyBox.TabIndex = 4;
            this.uiUploadClipboardKeyBox.TextValue = "";
            this.uiUploadClipboardKeyBox.UseSystemPasswordChar = false;
            // 
            // uiShowFileDropAreaLabel
            // 
            this.uiShowFileDropAreaLabel.AutoSize = true;
            this.uiShowFileDropAreaLabel.Location = new System.Drawing.Point(9, 180);
            this.uiShowFileDropAreaLabel.Name = "uiShowFileDropAreaLabel";
            this.uiShowFileDropAreaLabel.Size = new System.Drawing.Size(98, 13);
            this.uiShowFileDropAreaLabel.TabIndex = 2;
            this.uiShowFileDropAreaLabel.Text = "Show file drop area";
            // 
            // uiShowFileDropAreaKeyBox
            // 
            this.uiShowFileDropAreaKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiShowFileDropAreaKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiShowFileDropAreaKeyBox.Location = new System.Drawing.Point(12, 197);
            this.uiShowFileDropAreaKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiShowFileDropAreaKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiShowFileDropAreaKeyBox.Name = "uiShowFileDropAreaKeyBox";
            this.uiShowFileDropAreaKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiShowFileDropAreaKeyBox.TabIndex = 5;
            this.uiShowFileDropAreaKeyBox.TextValue = "";
            this.uiShowFileDropAreaKeyBox.UseSystemPasswordChar = false;
            // 
            // uiOpenStorageExplorer
            // 
            this.uiOpenStorageExplorer.AutoSize = true;
            this.uiOpenStorageExplorer.Location = new System.Drawing.Point(9, 222);
            this.uiOpenStorageExplorer.Name = "uiOpenStorageExplorer";
            this.uiOpenStorageExplorer.Size = new System.Drawing.Size(111, 13);
            this.uiOpenStorageExplorer.TabIndex = 2;
            this.uiOpenStorageExplorer.Text = "Open storage explorer";
            // 
            // uiSaveLocalScreenshotLabel
            // 
            this.uiSaveLocalScreenshotLabel.AutoSize = true;
            this.uiSaveLocalScreenshotLabel.Location = new System.Drawing.Point(9, 264);
            this.uiSaveLocalScreenshotLabel.Name = "uiSaveLocalScreenshotLabel";
            this.uiSaveLocalScreenshotLabel.Size = new System.Drawing.Size(112, 13);
            this.uiSaveLocalScreenshotLabel.TabIndex = 2;
            this.uiSaveLocalScreenshotLabel.Text = "Save local screenshot";
            // 
            // uiSaveLocalScreenAreaLabel
            // 
            this.uiSaveLocalScreenAreaLabel.AutoSize = true;
            this.uiSaveLocalScreenAreaLabel.Location = new System.Drawing.Point(9, 306);
            this.uiSaveLocalScreenAreaLabel.Name = "uiSaveLocalScreenAreaLabel";
            this.uiSaveLocalScreenAreaLabel.Size = new System.Drawing.Size(116, 13);
            this.uiSaveLocalScreenAreaLabel.TabIndex = 2;
            this.uiSaveLocalScreenAreaLabel.Text = "Save local screen area";
            // 
            // uiSaveLocalClipboardLabel
            // 
            this.uiSaveLocalClipboardLabel.AutoSize = true;
            this.uiSaveLocalClipboardLabel.Location = new System.Drawing.Point(9, 348);
            this.uiSaveLocalClipboardLabel.Name = "uiSaveLocalClipboardLabel";
            this.uiSaveLocalClipboardLabel.Size = new System.Drawing.Size(103, 13);
            this.uiSaveLocalClipboardLabel.TabIndex = 2;
            this.uiSaveLocalClipboardLabel.Text = "Save local clipboard";
            // 
            // uiOpenStorageExplorerKeyBox
            // 
            this.uiOpenStorageExplorerKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiOpenStorageExplorerKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiOpenStorageExplorerKeyBox.Location = new System.Drawing.Point(12, 239);
            this.uiOpenStorageExplorerKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiOpenStorageExplorerKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiOpenStorageExplorerKeyBox.Name = "uiOpenStorageExplorerKeyBox";
            this.uiOpenStorageExplorerKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiOpenStorageExplorerKeyBox.TabIndex = 6;
            this.uiOpenStorageExplorerKeyBox.TextValue = "";
            this.uiOpenStorageExplorerKeyBox.UseSystemPasswordChar = false;
            // 
            // uiSaveLocalScreenshotKeyBox
            // 
            this.uiSaveLocalScreenshotKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiSaveLocalScreenshotKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiSaveLocalScreenshotKeyBox.Location = new System.Drawing.Point(12, 281);
            this.uiSaveLocalScreenshotKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiSaveLocalScreenshotKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiSaveLocalScreenshotKeyBox.Name = "uiSaveLocalScreenshotKeyBox";
            this.uiSaveLocalScreenshotKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiSaveLocalScreenshotKeyBox.TabIndex = 7;
            this.uiSaveLocalScreenshotKeyBox.TextValue = "";
            this.uiSaveLocalScreenshotKeyBox.UseSystemPasswordChar = false;
            // 
            // uiSaveLocalScreenAreaKeyBox
            // 
            this.uiSaveLocalScreenAreaKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiSaveLocalScreenAreaKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiSaveLocalScreenAreaKeyBox.Location = new System.Drawing.Point(12, 323);
            this.uiSaveLocalScreenAreaKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiSaveLocalScreenAreaKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiSaveLocalScreenAreaKeyBox.Name = "uiSaveLocalScreenAreaKeyBox";
            this.uiSaveLocalScreenAreaKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiSaveLocalScreenAreaKeyBox.TabIndex = 8;
            this.uiSaveLocalScreenAreaKeyBox.TextValue = "";
            this.uiSaveLocalScreenAreaKeyBox.UseSystemPasswordChar = false;
            // 
            // uiSaveLocalClipboardKeyBox
            // 
            this.uiSaveLocalClipboardKeyBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.uiSaveLocalClipboardKeyBox.Key = System.Windows.Forms.Keys.None;
            this.uiSaveLocalClipboardKeyBox.Location = new System.Drawing.Point(12, 365);
            this.uiSaveLocalClipboardKeyBox.MinimumSize = new System.Drawing.Size(20, 20);
            this.uiSaveLocalClipboardKeyBox.Modifiers = System.Windows.Forms.Keys.None;
            this.uiSaveLocalClipboardKeyBox.Name = "uiSaveLocalClipboardKeyBox";
            this.uiSaveLocalClipboardKeyBox.Size = new System.Drawing.Size(210, 20);
            this.uiSaveLocalClipboardKeyBox.TabIndex = 9;
            this.uiSaveLocalClipboardKeyBox.TextValue = "";
            this.uiSaveLocalClipboardKeyBox.UseSystemPasswordChar = false;
            // 
            // HotkeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 397);
            this.Controls.Add(this.uiSaveLocalClipboardKeyBox);
            this.Controls.Add(this.uiShowFileDropAreaKeyBox);
            this.Controls.Add(this.uiSaveLocalScreenAreaKeyBox);
            this.Controls.Add(this.uiUploadClipboardKeyBox);
            this.Controls.Add(this.uiSaveLocalScreenshotKeyBox);
            this.Controls.Add(this.uiUploadScreenAreaKeyBox);
            this.Controls.Add(this.uiOpenStorageExplorerKeyBox);
            this.Controls.Add(this.uiUploadScreenshotKeyBox);
            this.Controls.Add(this.uiUploadFileKeyBox);
            this.Controls.Add(this.uiSaveLocalClipboardLabel);
            this.Controls.Add(this.uiShowFileDropAreaLabel);
            this.Controls.Add(this.uiSaveLocalScreenAreaLabel);
            this.Controls.Add(this.uiUploadClipbardLabel);
            this.Controls.Add(this.uiSaveLocalScreenshotLabel);
            this.Controls.Add(this.uiUploadScreenAreaLabel);
            this.Controls.Add(this.uiOpenStorageExplorer);
            this.Controls.Add(this.uiUploadScreenshotLabel);
            this.Controls.Add(this.uiUploadFileLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HotkeyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Hotkeys";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label uiUploadFileLabel;
        private DarkControls.DarkKeyBox uiUploadFileKeyBox;
        private System.Windows.Forms.Label uiUploadScreenshotLabel;
        private DarkControls.DarkKeyBox uiUploadScreenshotKeyBox;
        private System.Windows.Forms.Label uiUploadScreenAreaLabel;
        private DarkControls.DarkKeyBox uiUploadScreenAreaKeyBox;
        private System.Windows.Forms.Label uiUploadClipbardLabel;
        private DarkControls.DarkKeyBox uiUploadClipboardKeyBox;
        private System.Windows.Forms.Label uiShowFileDropAreaLabel;
        private DarkControls.DarkKeyBox uiShowFileDropAreaKeyBox;
        private System.Windows.Forms.Label uiOpenStorageExplorer;
        private System.Windows.Forms.Label uiSaveLocalScreenshotLabel;
        private System.Windows.Forms.Label uiSaveLocalScreenAreaLabel;
        private System.Windows.Forms.Label uiSaveLocalClipboardLabel;
        private DarkControls.DarkKeyBox uiOpenStorageExplorerKeyBox;
        private DarkControls.DarkKeyBox uiSaveLocalScreenshotKeyBox;
        private DarkControls.DarkKeyBox uiSaveLocalScreenAreaKeyBox;
        private DarkControls.DarkKeyBox uiSaveLocalClipboardKeyBox;
    }
}